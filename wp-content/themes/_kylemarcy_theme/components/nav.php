<?php 

require get_template_directory() . '/classes/NavWalker.php';
wp_nav_menu(array(
	'container' => 'nav',
	'container_class' => 'nav',
	'items_wrap' => '<ul class="nav-menu">%3$s</ul>',
	'walker' => new NavWalker(),
	'echo' => false,
));

?>

<header id="header">
	
	<nav id="mobilenav">
		
		<a href="<?php echo site_url(); ?>" class="mobilenav-logo">KyleMarcy</a>
		
		<div class="mobilenav-navicon--wrapper">
			<i class="fa fa-navicon centerer mobilenav-navicon"></i>
			<i class="fa fa-times centerer mobilenav-navicon-close"></i>
		</div>
	</nav>

	<div class="mobilenav-panel">
		<ul class="mobilenav-panel-item--wrapper">
			<li class="mobilenav-panel-item"><a id="toplink" href="#"><i class="fa fa-home"></i></a></li>
			<li class="mobilenav-panel-item"><a id="aboutlink" href="#">About</a></li>
			<li class="mobilenav-panel-item"><a id="portfoliolink" href="#">Portfolio</a></li>
			<li class="mobilenav-panel-item"><a id="contactlink" href="#">Contact</a></li>
		</ul>
	</div>
	


</header>