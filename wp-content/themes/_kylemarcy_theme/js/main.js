// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
var APP = {
	isElInView: function(el){
		if( typeof jQuery === 'function' && el instanceof jQuery){
			el = el[0];
		}
		var rect = el.getBoundingClientRect();
		return (
			rect.top >= 0 &&
			rect.left >= 0 &&
			rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
			rect.right <= (window.innerWidth || document.documentElement.clientWidth)
		);
	},
	isElHalfInView: function(el){
		if( typeof jQuery === 'function' && el instanceof jQuery){
			el = el[0];
		}
		var rect = el.getBoundingClientRect();
		var slice = el.clientHeight / 2;
		return (
			rect.top >= ( 0 - slice ) &&
			rect.left >= 0 &&
			rect.bottom <= (window.innerHeight + slice || document.documentElement.clientHeight + slice) &&
			rect.right <= (window.innerWidth || document.documentElement.clientWidth)
		);
	},
};
var Site = {};
var Kit = {};
;(function ( $, APP, Site, Kit, window, document, undefined ) {
	$(document).ready(function(){
		

		/*
			Categories of Tabs, Done Right!
		 */
		Site.DemoTabs = {
			/*
				Vars
			 */
				// wrapper for all the cats and tabs
				// category wrappers
				catWrappers: $('.tabs-category'),
				// category names
				// tabs (global index)
				anyTab: $('.tabs-category-tab'),
				// tabs (categorical index)
				anyPanel: $('.panels-panel'),
			/* 
				Methods
			*/
			_revealThePanel: function(e, tabIndexGlobal){
				// reveal linked panel
				$(Site.DemoTabs.anyPanel).removeClass('panels-panel--active');
				$(Site.DemoTabs.anyPanel[tabIndexGlobal]).addClass('panels-panel--active');
				// apply active style to clicked tab
				$(Site.DemoTabs.anyTab).removeClass('tabs-category-tab--active');
				$(e.target).addClass('tabs-category-tab--active');
			},
			_clickedTab: function(e){
				// global tab index
				var tabIndexGlobal = $('.tabs-category-tab').index(e.target);
				Site.DemoTabs._revealThePanel(e, tabIndexGlobal);
			},
			_hoveredTab: function(e){
				$(e.target).toggleClass('tabs-category-tab--hovered');
			},
			/*
				Initialize (run)
			 */
			_init: function(){

				$(Site.DemoTabs.anyPanel).first().addClass('panels-panel--active');
				$(Site.DemoTabs.anyTab).first().addClass('tabs-category-tab--active');

				// when you hover over a tab
				$(Site.DemoTabs.anyTab).hover(Site.DemoTabs._hoveredTab);
				// when you click a tab
				$(Site.DemoTabs.anyTab).on('click', Site.DemoTabs._clickedTab);
			}
		}
		Site.DemoTabs._init();










		Site.ContactForm = {
			
			submit: $('.wpcf7-submit'),
			inputs: $('.wpcf7-form-control:not(".wpcf7-submit")'),
			_submission: function(e){
				if( e.detail.status == 'validation_failed' ){
					Site.ContactForm.submit.addClass('cf7-submitfailed');
					Site.ContactForm.submit[0].value = 'Try Sending Again?';
				}
				if( e.detail.status == 'mail_failed' ){
					Site.ContactForm.submit.addClass('cf7-submitfailed');
					Site.ContactForm.submit[0].value = 'Sorry, Can\'t Send';
				}
				if(e.detail.status == 'mail_sent' || e.detail.status == 'mail_sent_ok'){
					Site.ContactForm.submit.addClass('cf7-submitsent');
					Site.ContactForm.submit[0].value = 'SENT! Thanks!';
				}

				// Clean Up
				Site.ContactForm.submit.removeClass('cf7-submitfailed');
				Site.ContactForm.submit.removeClass('cf7-submitsent');
				for (var i = 0; i < Site.ContactForm.inputs.length; i++) {
					$(Site.ContactForm.inputs[i]).removeClass('input--filled');
				}
			},
			_inputHandler:function(e){
				// style inputs w/ text
				( $(e.target).val() )						?
					$(e.target).addClass('input--filled')	:
					$(e.target).removeClass('input--filled');
			},
			_init: function(){
				document.addEventListener('wpcf7submit', Site.ContactForm._submission);
				document.addEventListener('change', Site.ContactForm._inputHandler);
			}
		}
		Site.ContactForm._init();


		
		Site.HeroFader = {
			bg: $('.hero-bg'),
			_runAnimation: function(){
				window.setInterval(function(){
					Site.HeroFader.bg.toggleClass('hero-bg--alternate');
				}, 6000);
			},
			_init: function(){
				$(window).on('load', Site.HeroFader._runAnimation);
			}
		}
		Site.HeroFader._init();







		APP.Parallax = {
			strength : 25,
			stronger : 40,
			imagesections : $('.parallaxbg'),
			_init : function(){
				$(window).on('resize scroll load', APP.Parallax._resizeLoadScrollHandler);
			},
			_map : function(n,i,o,r,t){return i>o?i>n?(n-i)*(t-r)/(o-i)+r:r:o>i?o>n?(n-i)*(t-r)/(o-i)+r:t:void 0;},
			_getAmount : function(el, strength){
				var windowcenter = $(window).scrollTop() + ( $(window).height() / 2 );
				var sectioncenter = $(el).offset().top + ($(el).height() / 2);
				var sectiondelta = windowcenter - sectioncenter;
				var scale = APP.Parallax._map(sectiondelta, 0, $(window).height(), 0, strength);
				return -50 + scale + '%';
			},
			_resizeLoadScrollHandler : function(){
				window.requestAnimationFrame(drawFrame);
				function drawFrame(){
					for(var i = 0; i < APP.Parallax.imagesections.length; i++){
						$(APP.Parallax.imagesections[i]).css('transform', 'translate3d(-50%, ' + APP.Parallax._getAmount(APP.Parallax.imagesections[i], APP.Parallax.strength) + ',0)');
					}
				}
			}
		}
		APP.Parallax._init();












		// Hero 'TagTyper'
		Site.TagTyper = {	
			tags: [
				'Creative',
				'Professional'
			],
			underscore: $('.hero-tagtyper-tag-underscore'),
			txtfield: $('.hero-tagtyper-tag-text'),
			_loadHandler: function(){
			},
			_init:function(){
				$(window).on('load', Site.TagTyper._loadHandler);
			}
		}
		// Site.TagTyper._init();

		var options = {
			strings: [
				'Creative',
				'Professional',
				'Responsive',
				'Mobile-First',
				'Friendly',
				'Organized',
				'Detail-Oriented',
				'Comprehensive',
			],
			startDelay: 1000,
			typeSpeed: 100,
			backDelay: 1000,
			backSpeed: 50,
			cursorChar: ' _',
			shuffle: true,
			smartBackspace: false,
			loop: true
		}
		// var typed = new Typed('.hero-tagtyper-text', options);
		Site.portfolio = {
			posts: $('.portfolio-posts-post'),
			_clickHandler: function(){
				var anchor = $(this).find('a.post-siteurl');
				var url = anchor[0];
				url.click();
			},
			_init: function(){
				$(Site.portfolio.posts).on('click', Site.portfolio._clickHandler);
			}
		}
		Site.portfolio._init();

		Site.revealer = {
			els_to_reveal: $('.revealer'),
			_scrollResizeLoadHandler: function(){
				for (var i = 0; i < Site.revealer.els_to_reveal.length; i++) {
					( APP.isElHalfInView(Site.revealer.els_to_reveal[i]) )			?
						$(Site.revealer.els_to_reveal[i]).addClass('revealer--revealed')		:
						$(Site.revealer.els_to_reveal[i]).removeClass('revealer--revealed')			;
				}
			},
			_init: function(){
				$(window).on('scroll resize load', Site.revealer._scrollResizeLoadHandler);
			},
		}
		Site.revealer._init();





		Site.mobilenav = {

			navicon: $('.mobilenav-navicon'),
			navicon_wrapper: $('.mobilenav-navicon--wrapper'),
			times: $('.mobilenav-navicon-close'),
			panel: $('.mobilenav-panel'),
			_clickHandler: function(){

				// If Panel is Revealed ... Handle Panel
				( !$(Site.mobilenav.panel).hasClass('mobilenav-panel--revealed') )	? 
					$(Site.mobilenav.panel).addClass('mobilenav-panel--revealed')			:
						$(Site.mobilenav.panel).removeClass('mobilenav-panel--revealed')		;
				// If Panel is Revealed... Handle Navicon / Times
				( !$(Site.mobilenav.navicon_wrapper).hasClass('navicon--panel-revealed') )	? (
					$(Site.mobilenav.navicon).fadeOut(),
					$(Site.mobilenav.times).fadeIn(),
					$(Site.mobilenav.navicon_wrapper).addClass('navicon--panel-revealed')		):(
						$(Site.mobilenav.navicon).fadeIn(),
						$(Site.mobilenav.times).fadeOut(),
						$(Site.mobilenav.navicon_wrapper).removeClass('navicon--panel-revealed')	);
			},
			_resizeScrollLoadHandler: function(){
				if( $(Site.mobilenav.panel).hasClass('mobilenav-panel--revealed') ){
					$(Site.mobilenav.panel).removeClass('mobilenav-panel--revealed');
				}
				if( $(Site.mobilenav.navicon_wrapper).hasClass('navicon--panel-revealed') ){
					$(Site.mobilenav.navicon).fadeIn();
					$(Site.mobilenav.times).fadeOut();
					$(Site.mobilenav.navicon_wrapper).removeClass('navicon--panel-revealed');
				}

			},
			_init: function(){
				$(Site.mobilenav.navicon_wrapper).on('click', Site.mobilenav._clickHandler);
				$(window).on('resize scroll load', Site.mobilenav._resizeScrollLoadHandler);
			}
		}
		Site.mobilenav._init();




		// this is a global component used to determine which breakpoint we're at
		// to listen for the event do $(document).on('breakpoint', eventHandlerCallback);
		// callback accepts normal amount of params that a js event listener callback would have
		// but there's a custom property added to the event object called "name"
		// assuming in the callback the event object variable passed in is e: console.log(e.device);
		APP.Breakpoint = {
			name : '',
			_init : function(){
				$(window).on('resize load', APP.Breakpoint._resizeLoadHander);
			},
			_resizeLoadHander : function(){
				if( $(window).width() > 1024 && APP.Breakpoint.name != 'desktop' ){
					APP.Breakpoint.name = 'desktop';
					APP.Breakpoint._dispatchEvent();
				}
				else if( $(window).width() <= 1024 && $(window).width() > 640 && APP.Breakpoint.name != 'tablet' ){
					APP.Breakpoint.name = 'tablet';	
					APP.Breakpoint._dispatchEvent();
				}
				else if( $(window).width() < 641 && APP.Breakpoint.name != 'mobile' ){		
					APP.Breakpoint.name = 'mobile';		
					APP.Breakpoint._dispatchEvent();
				}
			},
			_dispatchEvent : function(){
				$(document).trigger($.Event('breakpoint', {device: APP.Breakpoint.name}));
			}
		}
		APP.Breakpoint._init();
	});

})( jQuery, APP, Site, Kit, window, document );

