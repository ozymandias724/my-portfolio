<section class="hero section">
	<div class="centerer">
		<div class="hero-title-wrapper">
			<h1 class="hero-title">Kyle <span>Marcy</span></h1>
			<h2 class="hero-tagline">WordPress Developer</h2>
		</div>
		<div class="hero-tagtyper">
			<i class="ion-ios-arrow-right hero-tagtyper-angle"></i>
			<span class="hero-tagtyper-text"></span>
		</div>
	</div>




	<!-- Shuffle BackGround Colors -->
	<div class="hero-bg"></div>
</section>