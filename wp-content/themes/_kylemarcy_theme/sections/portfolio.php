<section class="section portfolio">
<?php 
	$args = array(
		'post_type' => 'dev-portfolio',
		'posts_per_page' => -1,
		'order_by' => 'menu_order',
		'order' => 'ASC',
	);
	$posts = get_posts( $args );
	if(!empty($posts)) :			
		$count = wp_count_posts('dev-portfolio')->publish;
		$class = 'portfolio';
		($count % 2 == 0 ? $class = 'portfolio-posts--even' : $class = 'portfolio-posts--odd'); 
		echo '<div class="portfolio-posts ' . $class . '">';
		echo '<h1 class="portfolio-heading revealer">Portfolio</h1>';
		foreach ($posts as $post) :
			$featuredimage = get_the_post_thumbnail_url($post);
			$excerpt = get_the_excerpt( $post );
			$postlink = get_the_permalink( $post );
 ?>
		<div class="portfolio-posts-post revealer">
			<div class="portfolio-posts-post-overlaywrapper revealer">
				<a class="portfolio-posts-post-title" href="<?php echo $postlink; ?>"><?php echo $post->post_title; ?></a>
				<div class="portfolio-posts-post-excerpt"><?php echo $excerpt; ?></div>
			</div>

			<img class="portfolio-posts-post-bg" src="<?php echo $featuredimage; ?>" alt="">
			<a class="post-siteurl" target="_blank" href="<?php echo get_field('siteurl', $post); ?>"></a>
		</div>
<?php 
		endforeach;
		echo '</div>';
	endif;
 ?>
	 <img class="portfolio-bg section-bg parallaxbg" src="<?php echo get_template_directory_uri() . '/library/img/portfoliobg.png'; ?>" alt="">
	 <img class="portfolio-bg--mobile section-bg--mobile" src="<?php echo get_template_directory_uri() . '/library/img/portfoliobg.png'; ?>" alt="">
</section>