 <section class="section footer">
	
	<div class="footer-contactform-wrapper">	
		<h2 class="section-heading footer-heading revealer">Get in touch </h2>
		<?php echo do_shortcode( '[contact-form-7 id="27" title="Contact form 1"]' ); ?>
	</div>
	
 
	<div class="subfooter revealer">
		<p>&copy Copyright <?php echo date('Y'); ?></p>
		<p>KyleMarcy</p>
	</div>

	<img class="contact-bg section-bg parallaxbg" src="<?php echo get_template_directory_uri() . '/library/img/contactbg.png'; ?>" alt="">
	<img class="contact-bg--mobile section-bg--mobile" src="<?php echo get_template_directory_uri() . '/library/img/contactbg.png'; ?>" alt="">
 </section>