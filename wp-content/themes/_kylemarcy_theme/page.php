<?php get_header(); ?>
<!-- Tabs Done Right -->
<section id="tabspage">
	 <ul class="tabs">
	 	<?php 
	 		$categories = [
	 			'Category One' => [
	 				'Tab 1',
	 				'Tab 2',
	 				'Tab 3',
	 				'Tab 4'
	 			],
	 			'Category Two' => [
	 				'Tab 1',
	 				'Tab 2',
	 				'Tab 3',
	 				'Tab 4',
	 				'Tab 5',
	 				'Tab 6',
	 				'Tab 7'
	 			],
	 			'Category Three' => [
	 				'Tab 1',
	 				'Tab 2',
	 				'Tab 3',
	 				'Tab 4',
	 				'Tab 5'
	 			],
	 			'Category Four' => [
	 				'Tab 1',
	 				'Tab 2',
	 				'Tab 3'
	 			]
	 		];
	 		foreach( $categories as $category => $tabs ) : ?>
 				<ul class="tabs-category">
					<h3 class="tabs-category-name"><?php echo $category; ?></h3>
			<?php foreach( $tabs as $i => $tab) : ?>
	 				<li class="tabs-category-tab"><?php echo $tab; ?></li>
 			<?php endforeach; ?>
				</ul>
	 	 <?php endforeach; ?>
	 </ul>
	<article class="panels">
	 	<?php 
	 		foreach($categories as $category => $tabs) :
	 			foreach( $tabs as $i => $tab) :
 		 ?>
	 			<aside class="panels-panel">
	 				<h2 class="panels-panel-category"><?php echo $category; ?></h2>
	 				<p><?php echo $tab; ?></p>
					
					<?php if( $i + 1 == 1 ) : ?>
						<p>Well, this is one way to do things...</p>
					<?php endif; ?>
					<?php 
						switch ($i + 1) {
						 	case 1:
						 		echo '<p>This is another</p>';
						 		?>
						 		<p>And Yet Another</p>
						 		<?php
						 		break;
						 	default :
						 		// code here
						 		break;
						 }
					 ?>
	 			</aside>
	 	<?php 
	 		endforeach;
	 		endforeach;
		 ?>
	</article>
</section>


<?php get_footer(); ?>