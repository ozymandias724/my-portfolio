<?php 
	get_header();
	include locate_template( 'components/nav.php'); 
	if(have_posts()): 
 ?>		
	<section id="site">
	<?php 
		while(have_posts()): the_post();
			$thePostType = get_post_type();
			$prettyPostType = str_replace('dev-','', $thePostType);
	 ?>
			<section class="single-hero" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
				<div class="centerer">
					<h1 class="single-hero-title"><?php the_title(); ?></h1>
				</div>
			</section>
			<section class="single-main">
				<h1 class="single-main-title"><?php the_title(); ?></h1>
				<div class="single-main-excerpt"><?php the_excerpt(); ?></div>
				<div class="section-main-content">
					<?php the_content(); ?>
					<div class="single-meta">
						<p>Posted in: <a href="<?php echo get_post_type_archive_link($thePostType); ?>"><?php echo $prettyPostType; ?></a></p>
						<p>Posted by: <?php the_author(); ?> on <?php the_time('l, F j, Y'); ?></p>
						<?php previous_post_link(); ?>
						<?php next_post_link(); ?>
					</div>
				</div>
			</section>
	<?php 
		endwhile;
		include locate_template( 'sections/contact.php'); 
	 ?>
	 </section>
<?php
	endif;
	get_footer();
 ?>