<?php 
	function register_portfolio_posts() {
	
		$labels = array(
			'name'               => __( 'Dev Portfolio', 'text-domain' ),
			'singular_name'      => __( 'Portfolio Piece', 'text-domain' ),
			'add_new'            => _x( 'Add New Portfolio Piece', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Portfolio Piece', 'text-domain' ),
			'edit_item'          => __( 'Edit Portfolio Piece', 'text-domain' ),
			'new_item'           => __( 'New Portfolio Piece', 'text-domain' ),
			'view_item'          => __( 'View Portfolio Piece', 'text-domain' ),
			'search_items'       => __( 'Search Dev Portfolio', 'text-domain' ),
			'not_found'          => __( 'No Dev Portfolio found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Dev Portfolio found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Portfolio Piece:', 'text-domain' ),
			'menu_name'          => __( 'Dev Portfolio', 'text-domain' ),
		);
	
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => null,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'excerpt',
				'custom-fields',
				'trackbacks',
				// 'comments',
				// 'revisions',
				'page-attributes',
				'post-formats',
			),
		);
	
		register_post_type( 'dev-portfolio', $args );
	}
	add_action( 'init', 'register_portfolio_posts' );
 ?>