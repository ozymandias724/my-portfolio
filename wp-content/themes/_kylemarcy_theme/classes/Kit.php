 <?php 
	class Kit {
		// takes an array of strings, loads sections with those titles


		public static function load_sections($sections){
			foreach( $sections as $index => $section ) {
				include( locate_template( 'sections/' . $section . '.php' ) );
			}
		}
	}
 ?>