<?php 
	/*
		Registers Advanced Custom Fields
		https://www.advancedcustomfields.com/resources/register-fields-via-php/
		https://www.advancedcustomfields.com/resources/acf_add_options_page/
	*/
	if( function_exists('acf_add_local_field_group') ):




	acf_add_local_field_group(array(
		'key' => 'group_15okn253in',
		'title' => 'Custom Post Info',
		'fields' => array (),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'dev-portfolio',
				),
			),
		),
	));

	acf_add_local_field(array(
		'key' => 'field_23uoi5n2n',
		'label' => 'Site URL',
		'name' => 'siteurl',
		'type' => 'link',
		'return_format' => 'url',
		'parent' => 'group_15okn253in'
	));
	
	endif;
 ?>